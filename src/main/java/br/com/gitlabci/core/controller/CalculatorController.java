package br.com.gitlabci.core.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calculator")
public class CalculatorController {
	
	@GetMapping("/sum/{number1}/{number2}")
	private String sum (@PathVariable Double number1, 
					   	@PathVariable Double number2) {
		
		return "<h3>Resultado: </h3>" + (number1 + number2);
	}
}
