package br.com.gitlabci.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gitlabci.core.excepion.IntegrantNotFoundException;
import br.com.gitlabci.core.model.Integrant;
import br.com.gitlabci.core.repository.IntegrantRepository;

@RestController
@RequestMapping("/integrant")
public class IntegrantController {
	
	@Autowired
	IntegrantRepository repository;
	
	
	@GetMapping("/integrants")
	public List<Integrant> getAllIntegrants() {
		return (List<Integrant>) this.repository.findAll();
	}
	
	@GetMapping("/integrants/names")
	public String getAllIntegrantsNames() {
		return this.generateHtmlForIntegrants((List<Integrant>) this.repository.findAll());
	}
	
	@GetMapping("/name/{name}")
	public ResponseEntity<List<Integrant>> getIntegrantByName(@PathVariable String name) {
		
		List<Integrant> integrants = this.repository.findByName(name)
											 .orElseThrow(() -> new IntegrantNotFoundException(name));
		
		return ResponseEntity.ok().body(integrants);
	}

	
	private String generateHtmlForIntegrants(List<Integrant> integrants) {
		
		String contentPage = "";
		for (Integrant integrant : integrants) {
			contentPage = contentPage + "<h5>" + integrant.getName() + "</h5>";
		}
		
		return "<h2><u> Integrantes: </h2></u>" + contentPage;
	}
	
}
