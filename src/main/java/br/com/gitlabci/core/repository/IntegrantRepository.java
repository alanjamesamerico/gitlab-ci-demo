package br.com.gitlabci.core.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.gitlabci.core.model.Integrant;

public interface IntegrantRepository extends CrudRepository <Integrant, Long> {
	
	Optional<List<Integrant>> findByName(String name);
}
