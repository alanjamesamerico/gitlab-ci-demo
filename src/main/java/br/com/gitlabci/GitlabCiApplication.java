package br.com.gitlabci;


import java.time.LocalDateTime;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gitlabci.core.model.Integrant;
import br.com.gitlabci.core.repository.IntegrantRepository;

@RestController
@SpringBootApplication
public class GitlabCiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabCiApplication.class, args);
	}
	
	@RequestMapping(path = "/")
	public String home() {
		return "<h1>Application Runing!</h1>";
	}

	@Bean
    CommandLineRunner initDatabase(IntegrantRepository repository) {
		
        return args -> {
            repository.save( Integrant.builder().name("Alan James Limão")
		            							.age(30)
		            							.birthday(LocalDateTime.of(1989, 10, 26, 8, 0))
		            							.build());
            
            repository.save( Integrant.builder().name("Abner Américo")
												.age(20)
												.birthday(LocalDateTime.of(1989, 10, 26, 8, 0))
												.build());
            
            repository.save( Integrant.builder().name("Pâmela Müler")
												.age(21)
												.birthday(LocalDateTime.of(1989, 10, 26, 8, 0))
												.build());
        };
    }
	
}
