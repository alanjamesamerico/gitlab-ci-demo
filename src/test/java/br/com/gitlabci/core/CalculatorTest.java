package br.com.gitlabci.core;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import br.com.gitlabci.core.service.CalculatorService;

class CalculatorTest {

	CalculatorService calc = new CalculatorService();
	
	@Test
	void sumTest() {
		assertTrue(calc.sum(1d, 1d) == 2);
		System.out.println("Result: " + calc.sum(1d, 1d));
	}

}
